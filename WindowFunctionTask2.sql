WITH subcategory_sales AS (

    SELECT prod_subcategory,
        t.calendar_year AS order_year,
        SUM(s.amount_sold) AS total_sales,
        LAG(SUM(s.amount_sold), 1, 0) OVER (PARTITION BY prod_subcategory ORDER BY calendar_year) AS prev_year_sales
        
    FROM sh.sales s
	JOIN sh.products p USING(prod_id)
	JOIN sh.times t USING(time_id)
    WHERE t.calendar_year BETWEEN 1998 AND 2001
    GROUP BY prod_subcategory, t.calendar_year

)

SELECT DISTINCT prod_subcategory
FROM subcategory_sales
GROUP BY prod_subcategory
HAVING MIN(total_sales) > MAX(prev_year_sales);


-- --  Let's break down the code step by step:

-- -- 1. Creating a Subcategory Sales Summary View:
-- --     - The code begins by defining a Common Table Expression (CTE) named `subcategory_sales`.
-- --     - This CTE is used to calculate the total sales and the previous year's sales for each product subcategory within the years 1998 to 2001.

-- -- 2. Column Selection and Aggregation:
-- --     - The SELECT clause specifies the columns to be included in the result set:
-- --         - `prod_subcategory`: Represents the product subcategory.
-- --         - `t.calendar_year AS order_year`: Represents the calendar year of the order.
-- --         - `SUM(s.amount_sold) AS total_sales`: Calculates the total sales for each product subcategory in a specific year.
-- --         - `LAG(SUM(s.amount_sold), 1, 0) OVER (PARTITION BY prod_subcategory ORDER BY calendar_year) AS prev_year_sales`: 
-- --         Uses the LAG window function to retrieve the total sales for the previous year for each product subcategory.

-- -- 3. Table Joins and Filtering:
-- --     - The code performs inner joins between the `sales`, `products`, and `times` tables using their respective keys (`prod_id`, `time_id`) to retrieve the necessary data for sales, products, and time information.
-- --     - The WHERE clause filters the data based on the calendar year falling within the range of 1998 to 2001.

-- -- 4. Grouping and Partitioning:
-- --     - The GROUP BY clause groups the results by `prod_subcategory` and `t.calendar_year` to calculate the total sales for each product subcategory in a specific year.

-- -- 5. Window Function:
-- --     - The LAG window function is used to retrieve the total sales for the previous year for each product subcategory. 
-- --     The function is partitioned by `prod_subcategory` and ordered by `calendar_year`.



